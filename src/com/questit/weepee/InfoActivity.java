package com.questit.weepee;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class InfoActivity extends Activity implements HeaderButtons {

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_info);
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		this.getMenuInflater().inflate(R.menu.info, menu);

		return true;
	}

	@Override
	public void gotoWhat(final View view) {
		HeaderButtonsUtils.gotoWhat(this, view);
	}

	@Override
	public void gotoInfo(final View view) {
		// HeaderButtonsUtils.gotoInfo(this, view);
	}

	@Override
	public void gotoHome(final View view) {
		HeaderButtonsUtils.gotoHome(this, view);
	}

}
