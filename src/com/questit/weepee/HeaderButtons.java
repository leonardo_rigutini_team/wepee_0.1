package com.questit.weepee;

import android.view.View;

public interface HeaderButtons {

	public void gotoWhat(final View view);

	public void gotoInfo(final View view);

	public void gotoHome(final View view);

}
