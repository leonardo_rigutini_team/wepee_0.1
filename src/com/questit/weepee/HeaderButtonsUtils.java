package com.questit.weepee;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

public class HeaderButtonsUtils {

	static public void gotoHome(final Activity activity, final View view) {
		final Button button = (Button) activity
				.findViewById(R.id.HeaderHomeButton);
		final int violet = activity.getResources().getColor(R.color.violet);
		button.setBackgroundColor(violet);
		final Intent intent = new Intent(activity, MainActivity.class);
		activity.startActivity(intent);
	}

	static public void gotoWhat(final Activity activity, final View view) {
		final Button button = (Button) activity
				.findViewById(R.id.HeaderWhatButton);
		final int violet = activity.getResources().getColor(R.color.violet);
		button.setBackgroundColor(violet);
		final Intent intent = new Intent(activity, WhatActivity.class);
		activity.startActivity(intent);
	}

	static public void gotoInfo(final Activity activity, final View view) {
		final Button button = (Button) activity
				.findViewById(R.id.HeaderInfoButton);
		final int violet = activity.getResources().getColor(R.color.violet);
		button.setBackgroundColor(violet);
		final Intent intent = new Intent(activity, InfoActivity.class);
		activity.startActivity(intent);
	}

}
